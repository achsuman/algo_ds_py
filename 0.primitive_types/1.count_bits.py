def count_bits(x):
    num_bits = 0
    while x:
        x >>= 1
    return num_bits
count_bits(1)
